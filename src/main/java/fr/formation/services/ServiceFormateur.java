package fr.formation.services;

import java.util.List;

import fr.formation.buisness.beans.Formation;
import fr.formation.dao.IDao;

public class ServiceFormateur implements IServiceFormation {
	private IDao dao ;
	
	public ServiceFormateur(IDao dao) {
		this.dao = dao;
	}
	
	@Override
	public List<Formation> getListFormationParIdPersonne(int id) {
		List<Formation> listeFormation = dao.getListeFormationParPersonne(id);
		return listeFormation;
	}

	@Override
	public void creerFormation(Formation f) {
		dao.creationFormation(f);
		
	}

	@Override
	public List<Formation> recupererListeFormation() {
		
		return dao.recupererAllFormation();
	}

	@Override
	public void inscriptionFormation(int idPersonne, int idFormation) {
		dao.inscriptionFormation(idPersonne, idFormation);
		
	}

	@Override
	public List<Formation> getListFormationParIdFormateur(int id) {
		return dao.getListeFormationParFormateur(id);
	}
	
	
	
	

}
