package fr.formation.buisness.traitement;

import java.util.HashMap;
import java.util.Map;

import fr.formation.commons.ObjetsCommuns;
import fr.formation.vo.PersonneVo;

public class CreationPersonneTraitement {
	
	private PersonneVo personne;
	private Map<String,String> mapErreur = new HashMap<String, String>();
	
	public CreationPersonneTraitement(PersonneVo personne){
		this.personne = personne;
	}

	public Map<String, String> getErreurs() {
		return mapErreur;
	}

	
	public boolean isValideChamps(){
		
	    try {
			verifieChampsNomPrenom(personne.getNom());
		} catch (Exception e) {
			mapErreur.put(ObjetsCommuns.Param.CHAMP_NOM, e.getMessage());
		}
	    
		try {

			verifieChampsNomPrenom(personne.getPrenom());
		} catch (Exception e) {
			mapErreur.put(ObjetsCommuns.Param.CHAMP_PRENOM, e.getMessage());
		}
		  try {
				verifieChampsNomPrenom(personne.getEmail());
			} catch (Exception e) {
				mapErreur.put(ObjetsCommuns.Param.CHAMP_EMAIL, e.getMessage());
			}
		try {

			verifieMotsDePasse(personne.getMdp(), personne.getConfirmationMdp());
		} catch (Exception e) {
			mapErreur.put(ObjetsCommuns.Param.CHAMP_CONF, e.getMessage());
		}
	            
	
		return mapErreur.size()==0?true:false;
	}
	
	
	
	private void verifieChampsNomPrenom(String s) throws Exception{
		if(s.isEmpty()){
			throw new Exception("champs vide  ");
		}
		
		if( s.length()<3){
			throw new Exception("le nombre de caractere doit etre superieur a 3");
		}
		
	}
	
	private void verifieMotsDePasse( String motDePasse, String confirmation ) throws Exception{
	    if (motDePasse != null && motDePasse.trim().length() != 0 && confirmation != null && confirmation.trim().length() != 0) {
	        if (!motDePasse.equals(confirmation)) {
	            throw new Exception("Les mots de passe entres sont differents, merci de les saisir a nouveau.");
	        } else if (motDePasse.trim().length() < 3) {
	            throw new Exception("Les mots de passe doivent contenir au moins 3 caracteres.");
	        }
	    } else {
	        throw new Exception("Merci de saisir et confirmer votre mot de passe.");
	    }
	}

}
