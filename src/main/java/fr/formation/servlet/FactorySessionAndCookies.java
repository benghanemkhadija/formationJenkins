package fr.formation.servlet;

import java.util.Date;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.formation.commons.ObjetsCommuns;
import fr.formation.vo.PersonneVo;

public class FactorySessionAndCookies {
	private static int COOKIE_MAX_AGE = 60 * 60 * 24 * 30; // environ un mois

	public static void ajouterSession(HttpSession session,
			HttpServletRequest req, HttpServletResponse resp, Object p) {

		session.setAttribute(ObjetsCommuns.SessionBeans.SESSION_PERSONNE, p);

	}

	public static void removeCookies(HttpServletResponse resp) {

		/* Demande de suppression du cookie du navigateur */
		setCookie(resp, ObjetsCommuns.Cookies.COOKIES_CONNECT, "", 0);

	}

	public static void addCookies(HttpServletResponse resp) {

		Date date = new Date();
		/* Creation du cookie, et ajout a la reponse HTTP */
		setCookie(resp, ObjetsCommuns.Cookies.COOKIES_CONNECT, date.toString(),
				COOKIE_MAX_AGE);

	}

	public static void supprimerSession(HttpSession session,
			HttpServletRequest req, HttpServletResponse resp, Object p) {
		session.setAttribute(ObjetsCommuns.SessionBeans.SESSION_PERSONNE, null);
	}

	private static void setCookie(HttpServletResponse response, String nom,
			String valeur, int maxAge) {
		Cookie cookie = new Cookie(nom, valeur);
		cookie.setMaxAge(maxAge);
		response.addCookie(cookie);
	}

	public static String getCookieValue(HttpServletRequest request, String nom) {
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie != null && nom.equals(cookie.getName())) {
					return cookie.getValue();
				}
			}
		}
		return null;
	}

}
