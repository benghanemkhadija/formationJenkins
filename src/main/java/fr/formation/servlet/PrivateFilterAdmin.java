package fr.formation.servlet;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.formation.commons.ObjetsCommuns;
import fr.formation.vo.PersonneVo;

public class PrivateFilterAdmin implements Filter {

	  public static final String ACCES_CONNEXION  = "/connexion";
	 public static final String ACCES_PUBLIC  = "/public.jsp";
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		 /* Cast des objets request et response */
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        System.out.println("PrivateFilterAdmin");
        
        /* Recuperation de la session depuis la requete */
        HttpSession session = req.getSession();
        PersonneVo pers = (PersonneVo) session.getAttribute( ObjetsCommuns.SessionBeans.SESSION_PERSONNE );
        /**
         * Si l'objet utilisateur n'existe pas dans la session en cours, alors
         * l'utilisateur n'est pas connecte.
         */
        if ( session.getAttribute( ObjetsCommuns.SessionBeans.SESSION_PERSONNE ) == null ) {
           
            //si on active le pattern /* il yaura une boucle infini de redirection meme 
            //pour les pages public !!!  
            //  res.sendRedirect( req.getContextPath() + ACCES_PUBLIC );
        	
        	
        	 /* Redirection vers la page publique dans le cas ou la session nexiste pas  */
            request.getRequestDispatcher( ACCES_CONNEXION ).forward( req, res );
        	 
        
        } else if (pers.isFormateur()) {
            /* Affichage de la page restreinte */
            chain.doFilter( req, res );
        } else{
        	 
        	request.getRequestDispatcher( ACCES_CONNEXION ).forward( req, res );
        }
		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}
