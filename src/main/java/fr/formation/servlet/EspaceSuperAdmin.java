package fr.formation.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.formation.buisness.beans.Formation;
import fr.formation.commons.ObjetsCommuns;
import fr.formation.dao.DaoImpl;
import fr.formation.services.IServiceFormation;
import fr.formation.services.IServicePersonne;
import fr.formation.services.ServiceFormateur;
import fr.formation.services.ServicePersonne;
import fr.formation.vo.PersonneVo;

public class EspaceSuperAdmin extends HttpServlet {
	
	private IServicePersonne servicePersonne;
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		servicePersonne = new ServicePersonne(DaoImpl.getInstance());
		
		List<PersonneVo> listePersonneVo =  servicePersonne.getAllPersonne();
		
		 
		
		 req.setAttribute(ObjetsCommuns.Beans.ALL_PERSONNE , listePersonneVo); 
		 this.getServletContext().getRequestDispatcher( ObjetsCommuns.Url.VUE_ALL_PERSONNE ).forward( req, resp );
		
	
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
	    
	}
	

	
	
}
