<%@ page language="java" pageEncoding="ISO-8859-1" contentType="text/html;charset=ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<%@ page isErrorPage="true" %>

<%
  response.setStatus(200);
%>

<html>
	<head>
		<title>Erreur de mon appli</title>
	</head>
	<body>
		<h2>Erreur</h2>
		
		affichage de l'exeption par scripltet : L'exception suivante s'est produite : <%= exception.getMessage()%>
		
		<br>attention l'EL suivant ne peut pas marche : \${exception.message} 
		
		<br> il faut utiliser le pageContext"  :  ${pageContext.errorData.throwable}
		
		<br>
		<a href="<c:url value="/creationPersonneMVC"/>">Retour</a>
	</body>
</html>
