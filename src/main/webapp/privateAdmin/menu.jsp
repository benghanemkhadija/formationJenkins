<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false"%>

 <c:if test="${!empty sessionScope.sessionPersonne}">
  Vous etes connecte(e) avec l'adresse : ${sessionScope.sessionPersonne.email} <br>
  <a href="<c:url value="/deconnexion" />">deconnexion</a>
  <a href="<c:url value="/formateurServlet" />">Mes Formations</a>
  <a href="<c:url value="privateAdmin/creationFormation" />">Creer formation</a>
 </c:if>

  <br><br>
